class CommentsController < ApplicationController
  before_action :logged_in_user, only: :create

  def index
    @micropost = Micropost.find_by(id: params[:id])
    @comments = @micropost.comments.paginate(page: params[:page])
    @comment = @micropost.comments.build
  end

  def create
    @comment = current_user.comments.build(content: params[:comment][:content],
                                           picture: params[:comment][:picture],
                                           micropost_id: params[:micropost_id])
    if @comment.save
      flash[:success] = "Comment created!"
      redirect_to comments_path+"?id=#{params[:micropost_id]}"
    else
      @micropost = Micropost.find_by(id: params[:micropost_id])
      @comments = @micropost.comments.paginate(page: params[:page])
      render 'comments/index'
    end
  end
end
